#[allow(unused_variables)]
#[allow(unused_assignments)]
#[allow(unused_mut)]
#[allow(unused_parens)]

fn main() {
    let mut iteration: u32;
    let max_iterations: u32;
    let mut total_iteration_count: u64;
    let mut z_real: f64;
    let mut z_imag: f64;
    let mut w_real: f64;
    let mut w_imag: f64;
    let mut c_real: f64;
    let mut c_imag: f64;
    let mut bailout: bool;
    let mut maxed_out: bool;
    let mut line = "a";
    
    max_iterations = 1000000;
    total_iteration_count = 0;

    for pix_imag in (-20..20).rev() {
        c_imag = pix_imag as f64 / 20.0f64;
        line = "";
        for pix_real in (-40..40) {
            bailout = false;
            c_real = pix_real as f64 / 40.0f64 - 0.5f64;
            z_real = 0.0f64;
            z_imag = 0.0f64;
            
            iteration = 0;
            while (!bailout && (iteration < max_iterations)) {
                w_real = (z_real * z_real) - (z_imag * z_imag) + c_real;
                w_imag = (2.0f64 * z_real * z_imag) + c_imag;
                z_real = w_real;
                z_imag = w_imag;
                if (z_real * z_real + z_imag * z_imag >= 4.0f64) {bailout = true;}
                
                total_iteration_count += 1;

                iteration += 1;
            }
            print!("{}", compute_character(iteration));
        }
        println!(""); 
    } 
	println!("Total iterations: {}", total_iteration_count);
}

#[allow(unused_variables)]
#[allow(unused_assignments)]
#[allow(unused_mut)]
#[allow(unused_parens)]

fn compute_character(dwell: u32) -> String {
	if (dwell <= 4) {return String::from("O");}
	if ((dwell <= 8) && (dwell > 4)) {return String::from("o");}
	if ((dwell <= 16) && (dwell > 8)) {return String::from("°");}
	if ((dwell <= 32) && (dwell > 16)) {return String::from(".");}

	return String::from(" ");
}


